<?php
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;



if (! function_exists('view')) {

    function view($view = null, $data = [], $mergeData = [])
    {


        $pathsToTemplates = [__ROOT__ . '/templates'];
        $pathToCompiledTemplates = '/project2/cache/compiled';

        $filesystem = new Filesystem;
        $eventDispatcher = new Dispatcher(new Container);

        $viewResolver = new EngineResolver;
        $bladeCompiler = new BladeCompiler($filesystem, $pathToCompiledTemplates);
        $viewResolver->register('blade', function () use ($bladeCompiler) {
            return new CompilerEngine($bladeCompiler);
        });
        $viewResolver->register('php', function () {
            return new PhpEngine;
        });

        $viewFinder = new FileViewFinder($filesystem, $pathsToTemplates);

        $viewFactory = new Factory($viewResolver, $viewFinder, $eventDispatcher);

        return $viewFactory->make($view, $data, $mergeData)->render();
    }
}


