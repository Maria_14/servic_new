<?php

namespace Server\Eloquent;

use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;

class Encapsulator
{
    private static $conn;
    private function __construct() {}
    /**
     * Initialize capsule and store reference to connection
     */
    static public function init()
    {
        if (is_null(self::$conn)) {
            $capsule = new Capsule;
            $capsule->addConnection([
                'driver' => 'mysql',
                'host' => 'mysql',
                'database' => 'top_view',
                'username' => 'admin',
                'password' => 'admin',
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'options'  => array(\PDO::MYSQL_ATTR_LOCAL_INFILE => true)
            ]);
            $capsule->setEventDispatcher(new Dispatcher(new Container));
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
        }
    }
}