<?php
/**
* 
*/
namespace Server\Controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Server\Models\Articles;
use Illuminate\Http\Request;
class MainController
{
	
	public function index()
	{
        return view('main.index');
	}
    public function update(Request $request)
    {

        if(empty($request->article_id) || empty($request->topic_id)){
            return json_encode(array('error' => 'Empty param article_id or topic_id'));
        }


        $modelArticle = Articles::where('article_id', '=', $request->article_id)->first();


        if(!empty($modelArticle)) {
            $modelArticle->count_view = 0;
            $modelArticle->save();
        } else {
            $modelArticle = new Articles();
            $modelArticle->load([
                'article_id' =>  $request->article_id,
                'topic_id'   =>  $request->topic_id,
                'count_view' => 0
            ]);
            $modelArticle->save();
        }


        $mas = [
            'article_id' => $request->article_id,
            'count_view' => $modelArticle->count_view
        ];

        $topArticle  = DB::table('tmp_articles')
            ->select(['article_id', 'count_view'])
            ->where(['topic_id' => $request->topic_id])
            ->orderBy('count_view', 'DESC')
            ->limit(3)
            ->get();

        $mas[] = $topArticle;
        $this->send($mas);

        return json_encode(array('success' => true));

    }
    public function fixcount(Request $request)
    {

        if(empty($request->article_id) || empty($request->topic_id)){
            return json_encode(array('error' => 'Empty param article_id or topic_id'));
        }

        $modelArticle = Articles::where('article_id', '=', $request->article_id)->first();
        if(!empty($modelArticle)) {
            $modelArticle->count_view = $modelArticle->count_view + 1;
            $modelArticle->save();
        } else {
            $modelArticle = new Articles();
            $modelArticle->load([
                'article_id' => $request->article_id,
                'topic_id'   => $request->topic_id,
                'count_view' => 1
            ]);
            $modelArticle->save();
        }

        $mas = [
            'article_id'  => $request->article_id,
            'count_view'  => $modelArticle->count_view
        ];

        $topArticle  = DB::table('tmp_articles')
            ->select(['article_id', 'count_view'])
            ->where(['topic_id' => $request->topic_id])
            ->orderBy('count_view', 'DESC')
            ->limit(3)
            ->get();
        array_push($mas, $topArticle);
        $this->send($mas);


        return json_encode(array('success' => true));

    }
    public function send($data)
    {
        $options = array(
            'cluster' => 'eu',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '4f4af9d0ae2fe5c08c28',
            '61cb436ac37b711ea648',
            '655098',
            $options
        );

        $pusher->trigger('my-channel', 'my-event', $data);


    }
}