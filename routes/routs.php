<?php
use Illuminate\Routing\Router;

$router->group(['namespace' => 'Server\Controllers'], function (Router $router) {
    $router->match(['get','post'], '/', 'MainController@index');
    $router->match(['get','post'], 'update', 'MainController@update');
    $router->match(['get','post'], 'fixcount', 'MainController@fixcount');
});
