<?php
session_start();
ini_set('memory_limit', '-1');

include './vendor/autoload.php';


use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Server\Eloquent\Encapsulator;
use Symfony\Component\HttpFoundation\Response;
use Server\Models\Articles;

define('__ROOT__', './app');
Encapsulator::init();

$container = new Container;
$request = Request::capture();
$container->instance('Illuminate\Http\Request', $request);
$events = new Dispatcher($container);

$router = new Router($events, $container);
require_once './routes/routs.php';

try {
    $response = $router->dispatch($request);
} catch (Symfony\Component\HttpKernel\Exception\HttpException $e) {

    $content = view('404');
    $response =  Response::create($content, 400, []);
}

$response->send();